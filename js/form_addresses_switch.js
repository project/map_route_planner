(function(Drupal, drupalSettings){

  'use strict';

  Drupal.behaviors.form_addresses_switch = {
    attach : function(context, settings) {
      if (drupalSettings.map_route.form_enable_element_btn_switch) {
        Drupal.behaviors.form_addresses_switch.initFormAddressesSwitch();
      }
    },
    switch_address: false,
    new_switch_address: null,
    initFormAddressesSwitch: function() {
      let addresses_reverse = document.getElementById('addresses-reverse'),
        location_input = document.getElementById('location-input');

      addresses_reverse.addEventListener('click', function () {
        let address0 = document.getElementById('address-0'),
          address1 = document.getElementById('address-1');

        // Change order display.
        address0.id = 'address-1';
        address1.id = 'address-0';

        // Make the test and store the value.
        let is_initial_state = address0.contains(location_input);
        Drupal.behaviors.form_addresses_switch.switch_address = is_initial_state;
      });
    },
    isAdressesSwitched: function() {
      let a = Drupal.behaviors.form_addresses_switch.switch_address,
        b = Drupal.behaviors.form_addresses_switch.new_switch_address;
      return a === b;
    },
    saveSwitchState: function () {
      let a = Drupal.behaviors.form_addresses_switch.switch_address;
      Drupal.behaviors.form_addresses_switch.new_switch_address = a;
    }
  };

})(Drupal, drupalSettings);
