(function(Drupal, drupalSettings){

  'use strict';

  Drupal.behaviors.form_geolocation_client_address = {
    initFormGeolocationClientAddress: function(autocompleteInput) {

      autocompleteInput.addEventListener('focus', (event) => {
        // Try HTML5 geolocation.
        if (autocompleteInput.value == '') {
          Drupal.behaviors.map_route.toggleOverlay('show');
          if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(
              (position) => {
                const latlng = {
                  lat: position.coords.latitude,
                  lng: position.coords.longitude,
                }, geocoder = new google.maps.Geocoder();

                geocoder.geocode({ location: latlng }).then((response) => {
                  if (response.results[0]) {
                    // Set place and input value.
                    let client_address_place = response.results[0];
                    Drupal.behaviors.google_map.client_address_place = client_address_place;
                    autocompleteInput.value = client_address_place.formatted_address;
                    Drupal.behaviors.map_route.toggleOverlay('hide');
                  } else {
                    window.alert('No results found');
                    Drupal.behaviors.map_route.toggleOverlay('hide');
                  }
                }).catch((e) => window.alert('Geocoder failed due to: ' + e));
              },
              () => {
                console.log('Error: The Geolocation service failed.');
                Drupal.behaviors.map_route.toggleOverlay('hide');
              }
            );
          }
          else {
            // Browser doesn't support Geolocation
            console.log("Error: Your browser doesn't support geolocation.");
            Drupal.behaviors.map_route.toggleOverlay('hide');
          }
        }
      });

    }
  };

})(Drupal, drupalSettings);
