(function(Drupal, drupalSettings){

  'use strict';

  Drupal.behaviors.appearance = {
    attach : function(context, settings) {
      initAppearance();
    }
  };

  function initAppearance() {
    let appearance = drupalSettings.map_route.appearance;

    if (typeof appearance !== 'undefined' && appearance !== null) {
      let map_route = document.querySelector('#container-map-route');
      updateAppearance(map_route, appearance);
    }

  }

  function updateAppearance(map_route, appearance) {
    Object.entries(appearance).forEach(
      ([property, value]) => map_route.style.setProperty('--' + property, value)
    );
  }

})(Drupal, drupalSettings);
