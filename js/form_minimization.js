(function(Drupal, drupalSettings){

  'use strict';

  Drupal.behaviors.form_minimization = {
    attach : function(context, settings) {
      if (drupalSettings.map_route.form_enable_element_btn_minimize_restore) {
        Drupal.behaviors.form_minimization.initFormMinimization();
      }
    },
    initFormMinimization: function() {
      let mini_btn = document.getElementById('minimize'),
        rest_btn = document.getElementById('restore');
      mini_btn.addEventListener('click', (event) => {
        Drupal.behaviors.form_minimization.minimize();
      });
      rest_btn.addEventListener('click', (event) => {
        Drupal.behaviors.form_minimization.restore();
      });
    },
    minimize: function() {
      let data = Drupal.behaviors.form_minimization.getData(),
        form = data.form,
        time = data.time;
      form.classList.add('minimizing');
      setTimeout(function(){
        form.classList.remove('minimizing');
        form.classList.add('minimized');
      }, time);
    },
    restore: function() {
      let data = Drupal.behaviors.form_minimization.getData(),
        form = data.form,
        time = data.time;
      form.classList.add('restoring');
      setTimeout(function(){
        form.classList.remove('minimized');
        form.classList.remove('restoring');
      }, time);
    },
    getData: function() {
      let destination_form = document.getElementById(
        'destination_form'
        ),
        form_style = getComputedStyle(destination_form),
        property_name = '--animation-minimize',
        form_animation_minimize = form_style.getPropertyValue(property_name),
        animation_time = parseFloat(form_animation_minimize) * 1000,
        results = {
          'form': destination_form,
          'time': animation_time
        };
      return results;
    }
  };

})(Drupal, drupalSettings);
