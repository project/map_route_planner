<?php

namespace Drupal\map_route_planner\Factory;

use Drupal\Core\Config\ConfigFactory;
use Drupal\map_route_planner\WsServiceConnector;

/**
 * Class GoogleMapsFactory.
 *
 * The Google Maps Factory.
 *
 * @package Drupal\map_route_planner\Service
 */
class GoogleMapsFactory extends WsServiceConnector {

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * Constructs a BinManager object.
   *
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   The configuration factory.
   */
  public function __construct(ConfigFactory $config_factory) {
    $this->configFactory = $config_factory;
  }

  public function getApiKey() {
    return $this->configFactory
      ->get('map_route_planner.settings')
      ->get('authentification.api_key');
  }

  public function getGeocoding(array $geocoding_parameters, string $api_key = '') {
    if ('' === $api_key) {
      $api_key = $this->getApiKey();
    }
    $parameters = "key=$api_key";

    if (!empty($geocoding_parameters['address'])) {
      $address = urlencode($geocoding_parameters['address']);
      $parameters .= "&address=$address";
    }
    elseif (
      !empty($geocoding_parameters['lat']) &&
      !empty($geocoding_parameters['lng'])
    ) {
      $lat = $geocoding_parameters['lat'];
      $lng = $geocoding_parameters['lng'];
      $parameters .= "&latlng=$lat,$lng";
    }

    return $this->requestWebService(
      "https://maps.googleapis.com/maps/api/geocode/json?$parameters"
    )['results'][0];
  }

}
