<?php

namespace Drupal\map_route_planner\Factory;

use Drupal\Component\Utility\Color;
use Drupal\Core\Config\ConfigFactory;

/**
 * The Map Route factory.
 */
class MapRouteFactory {
  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * The Google Maps Factory.
   *
   * @var \Drupal\map_route_planner\Factory\GoogleMapsFactory
   */
  protected $googleMapsFactory;

  /**
   * Constructs a BinManager object.
   *
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   The configuration factory.
   * @param \Drupal\map_route_planner\Factory\GoogleMapsFactory $google_maps_factory
   *   The Google Maps Factory.
   */
  public function __construct(ConfigFactory $config_factory, GoogleMapsFactory $google_maps_factory) {
    $this->configFactory = $config_factory;
    $this->googleMapsFactory = $google_maps_factory;
  }

  /**
   * Get all fields value by map_route, with the fieldgroup mentionned.
   *
   * @param $map_route
   *   The map_route entity.
   * @param array $allowed_first_key
   *   If empty, get all field_group. If not, keep only those mentionned.
   *
   * @return array
   *   The fields value for a map_route.
   */
  public function getFieldsValueByMapRoute($map_route, $allowed_first_key = []) {
    if (empty($allowed_first_key)) {
      $allowed_first_key = [
        'map',
        'marker',
        'form',
        'appearance',
      ];
    }
    $fields = $this->getMapRouteFieldsConfiguration();
    $fields_value = [];
    foreach ($fields as $field_first_key => $field_first_values) {
      if (in_array($field_first_key, $allowed_first_key)) {
        foreach ($field_first_values as $field_second_key => $field_second_values) {
          foreach ($field_second_values as $field_third_key) {
            // Get field name.
            $field_other_keys = $field_second_key . '_' . $field_third_key;
            $field_name = $field_first_key . '_' . $field_other_keys;

            // Get field object.
            $field = $map_route->get($field_name);

            // Get field value and set fields_value by field name.
            $field_value = $this->getFieldValueByFieldType($field);
            if (count($allowed_first_key) === 1) {
              $field_name = $field_other_keys;
            }
            $fields_value[$field_name] = $field_value;
          }
        }
      }
    }

    return $fields_value;
  }

  /**
   * Get map_route fields configuration.
   *
   * @return array
   *   Return the map_route fields configuration.
   */
  public function getMapRouteFieldsConfiguration() {
    return $this->getMapRouteConfigurations()->get('fields');
  }

  /**
   * @param $field
   * @return array|bool|mixed|string|string[]
   */
  public function getFieldValueByFieldType($field) {
    $field_type = $field->getFieldDefinition()->getType();

    // Get fields value by field type.
    $field_value = $field->getString();
    switch ($field_type) {
      case 'boolean':
        $field_value = $field_value == 1;
        break;

      case 'float':
      case 'integer':
        $function_val = $field_type == 'integer' ? 'intval' : 'floatval';
        $field_value = $function_val($field_value);
        break;

      case 'list_string':
        if (1 !== $field->getFieldDefinition()->getCardinality()) {
          $field_value = explode(
            ', ',
            $field_value
          );
        }

        $fieldName = $field->getFieldDefinition()->getName();
        if ('form_enable_element_transport' === $fieldName) {
          $new_field_value = [];
          foreach ($field_value as $transport_key) {
            switch ($transport_key) {
              case 'recommended':
                $transport_label = t('Recommended means of transport');
                break;

              case 'transit':
                $transport_label = t('By transit');
                break;

              case 'bike':
                $transport_label = t('By transit');
                break;

              case 'walk':
                $transport_label = t('On foot');
                break;

              case 'car':
              default:
                $transport_label = t('In car');
            }
            $new_field_value[$transport_key] = $transport_label;
          }
          $field_value = $new_field_value;
        }
        break;

      case 'color_field_type':
        $color_value = $field->getValue()[0];
        if (!empty($color_value['color'])) {
          $field_value = $color_value['color'];
          if (
            !isset($color_value['opacity']) ||
            (empty($color_value['opacity']) && '0' != $color_value['opacity'])
          ) {
            $color_value['opacity'] = '1';
          }
          if ('1' != $color_value['opacity']) {
            $color = Color::hexToRgb($color_value['color']);
            $r = $color['red'];
            $g = $color['green'];
            $b = $color['blue'];
            $a = $color_value['opacity'];
            $field_value = "rgba($r,$g,$b,$a)";
          }
        }
        break;
      case 'text_long':
        $field_value = current($field->getValue())['value'];
        break;

      case 'decimal':
      case 'string':
      default:
    }

    return $field_value;
  }

  /**
   * @param $entity
   * @param string[] $ancestor_fields
   */
  public function updateAppropriateLocationField($entity, $ancestor_fields = ['map_map_center', 'marker_marker_position']) {
    foreach ($ancestor_fields as $ancestor_field_name) {
      $location_type = $entity->get($ancestor_field_name . '_address_or_lat_lng')->getString();
      $address_field_name = $ancestor_field_name . '_address';
      $lat_field_name = $ancestor_field_name . '_lat';
      $lng_field_name = $ancestor_field_name . '_lng';
      switch ($location_type) {
        case 'address':
          $request_response = $this->googleMapsFactory->getGeocoding(
            ['address' => $entity->get($address_field_name)->getString()]
          );

          // Update entity fields.
          $lat = $request_response['geometry']['location']['lat'];
          $lng = $request_response['geometry']['location']['lng'];
          $entity->set($lat_field_name, $lat);
          $entity->set($lng_field_name, $lng);
          break;

        case 'lat_lng':
          $request_response = $this->googleMapsFactory->getGeocoding(
            [
              'lat' => $entity->get($lat_field_name)->getString(),
              'lng' => $entity->get($lng_field_name)->getString()
            ]
          );

          // Update entity field.
          $address = $request_response['formatted_address'];
          $entity->set(
            $address_field_name, $address
          );
          break;
      }
    }
  }

  /**
   * @return \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig
   */
  protected function getMapRouteConfigurations() {
    return $this->configFactory->getEditable('map_route_planner.config');
  }

}
