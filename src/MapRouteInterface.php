<?php

namespace Drupal\map_route_planner;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a map route entity type.
 */
interface MapRouteInterface extends ContentEntityInterface, EntityChangedInterface {

  /**
   * Gets the map route title.
   *
   * @return string
   *   Title of the map route.
   */
  public function getTitle();

  /**
   * Sets the map route title.
   *
   * @param string $title
   *   The map route title.
   *
   * @return \Drupal\map_route_planner\MapRouteInterface
   *   The called map route entity.
   */
  public function setTitle($title);

  /**
   * Gets the map route creation timestamp.
   *
   * @return int
   *   Creation timestamp of the map route.
   */
  public function getCreatedTime();

  /**
   * Sets the map route creation timestamp.
   *
   * @param int $timestamp
   *   The map route creation timestamp.
   *
   * @return \Drupal\map_route_planner\MapRouteInterface
   *   The called map route entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the map route status.
   *
   * @return bool
   *   TRUE if the map route is enabled, FALSE otherwise.
   */
  public function isEnabled();

  /**
   * Sets the map route status.
   *
   * @param bool $status
   *   TRUE to enable this map route, FALSE to disable.
   *
   * @return \Drupal\map_route_planner\MapRouteInterface
   *   The called map route entity.
   */
  public function setStatus($status);

}
