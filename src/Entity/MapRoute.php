<?php

namespace Drupal\map_route_planner\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\map_route_planner\MapRouteInterface;

/**
 * Defines the map route entity class.
 *
 * @ContentEntityType(
 *   id = "map_route",
 *   label = @Translation("Map route"),
 *   label_collection = @Translation("Map routes"),
 *   handlers = {
 *     "view_builder" = "Drupal\map_route_planner\MapRouteViewBuilder",
 *     "list_builder" = "Drupal\map_route_planner\MapRouteListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\map_route_planner\MapRouteAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\map_route_planner\Form\MapRouteForm",
 *       "edit" = "Drupal\map_route_planner\Form\MapRouteForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "map_route",
 *   data_table = "map_route_field_data",
 *   translatable = TRUE,
 *   admin_permission = "access map route overview",
 *   entity_keys = {
 *     "id" = "id",
 *     "langcode" = "langcode",
 *     "label" = "title",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form" = "/admin/config/content/map-route/add",
 *     "canonical" = "/map-route/{map_route}",
 *     "edit-form" = "/map-route/{map_route}/edit",
 *     "delete-form" = "/map-route/{map_route}/delete",
 *     "collection" = "/admin/config/content/map-route"
 *   },
 * )
 */
class MapRoute extends ContentEntityBase implements MapRouteInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    return $this->get('title')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTitle($title) {
    $this->set('title', $title);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isEnabled() {
    return (bool)$this->get('status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setStatus($status) {
    $this->set('status', $status);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setTranslatable(TRUE)
      ->setLabel(t('Title'))
      ->setDescription(t('The title of the map route entity.'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setDescription(t('The time that the map route was created.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 3,
      ])
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 3,
      ]);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the map route was last edited.'));

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Status'))
      ->setDescription(t('A boolean indicating whether the map route is enabled.'))
      ->setDefaultValue(TRUE)
      ->setSetting('on_label', 'Published')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => FALSE,
        ],
        'weight' => 4,
      ])
      ->setDisplayOptions('view', [
        'type' => 'boolean',
        'label' => 'above',
        'weight' => 4,
        'settings' => [
          'format' => 'enabled-disabled',
        ],
      ]);

    self::getMapFields($fields);
/*
    // Fields for centering map.
    $fields['map_map_center_address_or_lat_lng'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Set the default map center with a'))
      ->setDescription(t('You can choose the centering of the map with an address or geographical coordinates!'))
      ->setDefaultValue('address')
      ->setSettings([
        'allowed_values' => [
          'address' => t('Physical Address'),
          'lat_lng' => t('Coordinates (Latitude/Longitude)'),
        ],
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 21,
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'list_default',
        'weight' => 21,
      ])
      ->setRequired(TRUE);

    $fields['map_map_center_address'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Address'))
      ->setDefaultValue('32 rue de Cambrai, 75019, Paris')
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 22,
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => 22,
      ]);

    $fields['map_map_center_lat'] = BaseFieldDefinition::create('decimal')
      ->setLabel(t('Latitude'))
      ->setDefaultValue(48.8955888)
      ->setSetting('precision', 12)
      ->setSetting('scale', 8)
      ->setSetting('min', -90)
      ->setSetting('max', 90)
      //->setSetting('step', '0.00000001')
      //->setSetting('step', 'any')
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => 23,
        'settings' => [
          'placeholder' => '0.00',
        ],
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'number_decimal',
        'weight' => 23,
        'settings' => [
          'thousand_separator' => '',
          'decimal_separator' => '.',
          'precision' => 12,
          'scale' => 8,
        ],
      ])
      ->setCardinality(1);

    $fields['map_map_center_lng'] = BaseFieldDefinition::create('decimal')
      ->setLabel(t('Longitude'))
      ->setDefaultValue(2.3821658)
      ->setSetting('precision', 12)
      ->setSetting('scale', 8)
      ->setSetting('min', -180)
      ->setSetting('max', 180)
      //->setSetting('step', '0.00000001')
      //->setSetting('step', 'any')
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => 24,
        'settings' => [
          'placeholder' => '0.00',
        ],
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'number_decimal',
        'weight' => 24,
        'settings' => [
          'thousand_separator' => '',
          'decimal_separator' => '.',
          'precision' => 12,
          'scale' => 8,
        ],
      ])
      ->setCardinality(1);

    $fields['map_settings_map_enable_geoloc'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Enable geolocation'))
      ->setDescription(t('A boolean indicating whether the user is geolocated.'))
      ->setDefaultValue(TRUE)
      ->setSetting('on_label', 'Enable')
      ->setSetting('off_label', 'Disable')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => TRUE,
        ],
        'weight' => 25,
      ])
      ->setDisplayOptions('view', [
        'type' => 'boolean',
        'label' => 'above',
        'weight' => 25,
        'settings' => [
          'format' => 'enabled-disabled',
        ],
      ]);

    $fields['map_settings_map_map_type'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Map type'))
      ->setDefaultValue('terrain')
      ->setSettings([
        'allowed_values' => [
          'roadmap' => t('RoadMap'),
          'satellite' => t('Satellite'),
          'hybrid' => t('Hybrid'),
          'terrain' => t('Terrain'),
        ],
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 26,
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'list_default',
        'weight' => 26,
      ])
      ->setRequired(TRUE);

    $fields['map_settings_map_zoom_scroll'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Enable Zoom scrolling'))
      ->setDescription(t('A boolean indicating whether the map route is zoom scrollable.'))
      ->setDefaultValue(TRUE)
      ->setSetting('on_label', 'Enable')
      ->setSetting('off_label', 'Disable')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => TRUE,
        ],
        'weight' => 27,
      ])
      ->setDisplayOptions('view', [
        'type' => 'boolean',
        'label' => 'hidden',
        'weight' => 27,
        'settings' => [
          'format' => 'enabled-disabled',
        ],
      ]);

    $zoom_max = 21;
    $fields['map_settings_map_zoom_max'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Zoom maximum authorized'))
      ->setDefaultValue($zoom_max)
      ->setSetting('unsigned', FALSE)
      ->setSetting('size', 'normal')
      ->setSetting('min', 0)
      ->setSetting('max', $zoom_max)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'number_integer',
        'weight' => 28,
      ])
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => 28,
      ])
      ->setCardinality(1)
      ->setRequired(TRUE);

    $fields['map_settings_map_zoom'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Default zoom'))
      ->setDefaultValue(16)
      ->setSetting('unsigned', FALSE)
      ->setSetting('size', 'normal')
      ->setSetting('min', 0)
      ->setSetting('max', $zoom_max)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'number_integer',
        'weight' => 29,
      ])
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => 29,
      ])
      ->setCardinality(1)
      ->setRequired(TRUE);

    // Fields for placing marker.
    $fields['marker_marker_position_address_or_lat_lng'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Set the default marker position with a'))
      ->setDescription(t('You can choose the position of the marker with an address or geographical coordinates!'))
      ->setDefaultValue('address')
      ->setSettings([
        'allowed_values' => [
          'address' => t('Physical Address'),
          'lat_lng' => t('Coordinates (Latitude/Longitude)'),
        ],
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 30,
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'list_default',
        'weight' => 30,
      ])
      ->setRequired(TRUE);

    $fields['marker_marker_position_address'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Address'))
      ->setDefaultValue('32 rue de Cambrai, 75019, Paris')
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 31,
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => 31,
      ]);

    $fields['marker_marker_position_lat'] = BaseFieldDefinition::create('decimal')
      ->setLabel(t('Latitude'))
      ->setDefaultValue(48.8955888)
      ->setSetting('precision', 12)
      ->setSetting('scale', 8)
      ->setSetting('min', -90)
      ->setSetting('max', 90)
      //->setSetting('step', '0.00000001')
      //->setSetting('step', 'any')
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => 32,
        'settings' => [
          'placeholder' => '0.00',
        ],
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'number_decimal',
        'weight' => 32,
        'settings' => [
          'thousand_separator' => '',
          'decimal_separator' => '.',
          'precision' => 12,
          'scale' => 8,
        ],
      ])
      ->setCardinality(1);

    $fields['marker_marker_position_lng'] = BaseFieldDefinition::create('decimal')
      ->setLabel(t('Longitude'))
      ->setDefaultValue(2.3821658)
      ->setSetting('precision', 12)
      ->setSetting('scale', 8)
      ->setSetting('min', -180)
      ->setSetting('max', 180)
      //->setSetting('step', '0.00000001')
      //->setSetting('step', 'any')
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => 33,
        'settings' => [
          'placeholder' => '0.00',
        ],
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'number_decimal',
        'weight' => 33,
        'settings' => [
          'thousand_separator' => '',
          'decimal_separator' => '.',
          'precision' => 12,
          'scale' => 8,
        ],
      ])
      ->setCardinality(1);

    $fields['marker_info_title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Marker Title'))
      ->setDefaultValue('Trained People')
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 34,
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => 34,
      ])
      ->setTranslatable(TRUE)
      ->setCardinality(1);

    $fields['marker_info_enable_window_description'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Enable Info Window'))
      ->setDefaultValue(TRUE)
      ->setSetting('on_label', 'Enable')
      ->setSetting('off_label', 'Disable')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => TRUE,
        ],
        'weight' => 35,
      ])
      ->setDisplayOptions('view', [
        'type' => 'boolean',
        'label' => 'above',
        'weight' => 35,
        'settings' => [
          'format' => 'enabled-disabled',
        ],
      ]);

    $fields['marker_info_window_description'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Info Window Description'))
      ->setDefaultValue("<h1><strong>Trained People</strong></h1><p>The best Drupal learning organization</p><p><a href='https://www.drupalfrance.com/' target='_blank'>Discover Drupal</a></p>")
      // @todo : @nono : set full_html by default not worked.
      // ->setDefaultValue("<h1><strong>Trained People</strong></h1><p>The best Drupal learning organization</p><p><a href='https://www.drupalfrance.com/' target='_blank'>Discover Drupal</a></p>", 'full_html')
      ->setDisplayOptions('form', [
        'type' => 'text_textfield',
        'weight' => 36,
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'text_default',
        'weight' => 36,
      ])
      ->setTranslatable(TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);
*/

    // Fields for placing form.
    $fields['form_destination_title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Address title'))
      ->setDefaultValue(t('Trained People'))
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 37,
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => 37,
      ])
      ->setTranslatable(TRUE)
      ->setRequired(TRUE)
      ->setCardinality(1);

    $fields['form_destination_address'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Address'))
      ->setDefaultValue(t('32 rue de Cambrai, 75019, Paris'))
      ->setSetting('max_length', 500)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 38,
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => 38,
      ])
      ->setRequired(TRUE)
      ->setCardinality(1);

    $fields['form_enable_element_transport'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Choose available transport modes'))
      ->setDefaultValue([
        'recommended',
        'car',
        'transit',
        'bike',
        'walk',
      ])
      ->setSettings([
        'allowed_values' => [
          'recommended' => t('Recommended means of transport'),
          'car' => t('In car'),
          'transit' => t('By transit'),
          'bike' => t('On bike'),
          'walk' => t('On foot')
        ],
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_buttons',
        'weight' => 39,
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'list_default',
        'weight' => 39,
      ])
      /*->setRequired(TRUE)*/
      ->setCardinality(-1);

    $fields['form_enable_element_btn_switch'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Enable switch button'))
      ->setDefaultValue(TRUE)
      ->setSetting('on_label', 'Enable')
      ->setSetting('off_label', 'Disable')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => TRUE,
        ],
        'weight' => 40,
      ])
      ->setDisplayOptions('view', [
        'type' => 'boolean',
        'label' => 'hidden',
        'weight' => 40,
        'settings' => [
          'format' => 'enabled-disabled',
        ],
      ]);

    $fields['form_enable_element_btn_minimize_restore'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Enable minimize/restore form button'))
      ->setDefaultValue(TRUE)
      ->setSetting('on_label', 'Enable')
      ->setSetting('off_label', 'Disable')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => TRUE,
        ],
        'weight' => 41,
      ])
      ->setDisplayOptions('view', [
        'type' => 'boolean',
        'label' => 'hidden',
        'weight' => 41,
        'settings' => [
          'format' => 'enabled-disabled',
        ],
      ]);

    $fields['form_label_address_show_start'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Show start address label'))
      ->setDefaultValue(TRUE)
      ->setSetting('on_label', 'Enable')
      ->setSetting('off_label', 'Disable')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => TRUE,
        ],
        'weight' => 42,
      ])
      ->setDisplayOptions('view', [
        'type' => 'boolean',
        'label' => 'hidden',
        'weight' => 42,
        'settings' => [
          'format' => 'enabled-disabled',
        ],
      ]);

    $fields['form_label_address_text_start'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Customize text label for starting address'))
      ->setDefaultValue(t('Adresse de départ'))
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 43,
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => 43,
      ])
      ->setTranslatable(TRUE)
      ->setRequired(TRUE)
      ->setCardinality(1);

    $fields['form_label_address_show_end'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Show start address label'))
      ->setDefaultValue(TRUE)
      ->setSetting('on_label', 'Enable')
      ->setSetting('off_label', 'Disable')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => TRUE,
        ],
        'weight' => 44,
      ])
      ->setDisplayOptions('view', [
        'type' => 'boolean',
        'label' => 'hidden',
        'weight' => 44,
        'settings' => [
          'format' => 'enabled-disabled',
        ],
      ]);

    $fields['form_label_address_text_end'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Customize text label for ending address'))
      ->setDefaultValue(t("Adresse d'arrivée"))
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 45,
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => 45,
      ])
      ->setTranslatable(TRUE)
      ->setRequired(TRUE)
      ->setCardinality(1);

    $fields['form_other_options_starting_address_placeholder'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Customize text placeholder for starting address'))
      ->setDescription(t('Leave empty for no text'))
      ->setDefaultValue(t("Indiquez l'adresse de départ"))
      ->setSetting('max_length', 500)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 46,
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => 46,
      ])
      ->setTranslatable(TRUE)
      ->setCardinality(1);

    $fields['form_other_options_submit_button_label'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Customize text for submit button'))
      ->setDefaultValue(t("Afficher l'itinéraire"))
      ->setSetting('max_length', 500)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 47,
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => 47,
      ])
      ->setRequired(TRUE)
      ->setCardinality(1);

    // Fields for placing appearance.
    $fields['appearance_dimension_map_width_map'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Width'))
      ->setDefaultValue('100%')
      ->setSetting('max_length', 25)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 48,
        'settings' => [
          'size' => 5,
        ],
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => 48,
      ])
      ->setRequired(TRUE)
      ->setCardinality(1);

    $fields['appearance_dimension_map_height_map'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Height'))
      ->setDefaultValue('500px')
      ->setSetting('max_length', 25)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 49,
        'settings' => [
          'size' => 5,
        ],
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => 49,
      ])
      ->setRequired(TRUE)
      ->setCardinality(1);

    $fields['appearance_form_position_top_position'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Top position'))
      ->setDefaultValue('auto')
      ->setSetting('max_length', 25)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 50,
        'settings' => [
          'size' => 5,
        ],
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => 50,
      ])
      ->setRequired(TRUE)
      ->setCardinality(1);

    $fields['appearance_form_position_bottom_position'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Bottom position'))
      ->setDefaultValue('3px')
      ->setSetting('max_length', 25)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 51,
        'settings' => [
          'size' => 5,
        ],
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => 51,
      ])
      ->setRequired(TRUE)
      ->setCardinality(1);

    $fields['appearance_form_position_left_position'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Left position'))
      ->setDefaultValue('3px')
      ->setSetting('max_length', 25)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 52,
        'settings' => [
          'size' => 5,
        ],
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => 52,
      ])
      ->setRequired(TRUE)
      ->setCardinality(1);

    $fields['appearance_form_position_right_position'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Right position'))
      ->setDefaultValue('auto')
      ->setSetting('max_length', 25)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 53,
        'settings' => [
          'size' => 5,
        ],
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => 53,
      ])
      ->setRequired(TRUE)
      ->setCardinality(1);

    $fields['appearance_form_color_text_label_text_color'] = BaseFieldDefinition::create('color_field_type')
      ->setLabel(t('Text Color for Label Start & End'))
      ->setDefaultValue([
        'color' => '#3C3C3C',
        'opacity' => 1,
      ])
      ->setSetting('opacity', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'color_field_widget_html5',
        'weight' => 54,
        'settings' => [
          'size' => 5,
        ],
      ])
      ->setDisplayOptions('view', [
        'type' => 'color_field_formatter_text',
        'label' => 'hidden',
        'weight' => 54,
      ])
      ->setRequired(TRUE)
      ->setCardinality(1);

    $fields['appearance_form_color_text_submit_button_text_color'] = BaseFieldDefinition::create('color_field_type')
      ->setLabel(t('Text Color for Submit Button'))
      ->setDefaultValue([
        'color' => '#FFF',
        'opacity' => 1,
      ])
      ->setSetting('opacity', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'color_field_widget_html5',
        'weight' => 55,
      ])
      ->setDisplayOptions('view', [
        'type' => 'color_field_formatter_text',
        'label' => 'hidden',
        'weight' => 55,
      ])
      ->setRequired(TRUE)
      ->setCardinality(1);

    $fields['appearance_form_color_bg_header_color'] = BaseFieldDefinition::create('color_field_type')
      ->setLabel(t('Head Form Color'))
      ->setDefaultValue([
        'color' => '#CCC',
        'opacity' => 1,
      ])
      ->setSetting('opacity', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'color_field_widget_html5',
        'weight' => 56,
      ])
      ->setDisplayOptions('view', [
        'type' => 'color_field_formatter_text',
        'label' => 'hidden',
        'weight' => 56,
      ])
      ->setRequired(TRUE)
      ->setCardinality(1);

    $fields['appearance_form_color_bg_form_color'] = BaseFieldDefinition::create('color_field_type')
      ->setLabel(t('Form Color'))
      ->setDefaultValue([
        'color' => '#F7F7F7',
        'opacity' => 1,
      ])
      ->setSetting('opacity', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'color_field_widget_html5',
        'weight' => 57,
      ])
      ->setDisplayOptions('view', [
        'type' => 'color_field_formatter_text',
        'label' => 'hidden',
        'weight' => 57,
      ])
      ->setRequired(TRUE)
      ->setCardinality(1);

    $fields['appearance_form_color_bg_button_color'] = BaseFieldDefinition::create('color_field_type')
      ->setLabel(t('Button Color'))
      ->setDefaultValue([
        'color' => '#E30613',
        'opacity' => 1,
      ])
      ->setSetting('opacity', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'color_field_widget_html5',
        'weight' => 58,
      ])
      ->setDisplayOptions('view', [
        'type' => 'color_field_formatter_text',
        'label' => 'hidden',
        'weight' => 58,
      ])
      ->setRequired(TRUE)
      ->setCardinality(1);

    $fields['appearance_form_color_three_btn_three_btn_color'] = BaseFieldDefinition::create('color_field_type')
      ->setLabel(t('Button Color'))
      ->setDefaultValue([
        'color' => '#BDBDBD',
        'opacity' => 1,
      ])
      ->setSetting('opacity', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'color_field_widget_html5',
        'weight' => 59,
      ])
      ->setDisplayOptions('view', [
        'type' => 'color_field_formatter_text',
        'label' => 'hidden',
        'weight' => 59,
      ])
      ->setRequired(TRUE)
      ->setCardinality(1);

    $fields['appearance_form_color_three_btn_three_btn_hover_color'] = BaseFieldDefinition::create('color_field_type')
      ->setLabel(t('Button Hover Color'))
      ->setDefaultValue([
        'color' => '#787878',
        'opacity' => 1,
      ])
      ->setSetting('opacity', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'color_field_widget_html5',
        'weight' => 60,
      ])
      ->setDisplayOptions('view', [
        'type' => 'color_field_formatter_text',
        'label' => 'hidden',
        'weight' => 60,
      ])
      ->setRequired(TRUE)
      ->setCardinality(1);


    return $fields;
  }

  /**
   * En cours de dev/refacto.
   * Cette méthode est "finit" pour le moment.
   * Créer une autre méthode getFormFields,et faire la même (cf le fichier map_route_planner_v2.config.yml),
   * puis créer les autres méthodes.
   * @param $fields
   */
  public function getMapFields(&$fields) {

    // Settings destination address.
    $fields['map_settings_destination_address_title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Title'))
      ->setDefaultValue('Trained People')
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 0,
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => 0,
      ])
      ->setTranslatable(TRUE)
      ->setCardinality(1);

    $fields['map_settings_destination_address_description'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Description'))
      ->setDescription(t('A short description that will appear when you click on the marker on the map!'))
      ->setDefaultValue("<h1><strong>Trained People</strong></h1><p>The best Drupal learning organization</p><p><a href='https://www.drupalfrance.com/' target='_blank'>Discover Drupal</a></p>")
      ->setDisplayOptions('form', [
        'type' => 'text_textfield',
        'weight' => 1,
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'text_default',
        'weight' => 1,
      ])
      ->setTranslatable(TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['map_settings_destination_address_location_type'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Set the destination location'))
      ->setDescription(t('You can set the destination location with an address or geographical coordinates!'))
      ->setDefaultValue('address')
      ->setSettings([
        'allowed_values' => [
          'address' => t('Physical Address'),
          'lat_lng' => t('Coordinates (Latitude/Longitude)'),
        ],
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 2,
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'list_default',
        'weight' => 2,
      ])
      ->setRequired(TRUE);

    $fields['map_settings_destination_address_address'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Address'))
      ->setDefaultValue('32 rue de Cambrai, 75019, Paris')
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 3,
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => 3,
      ]);

    $fields['map_settings_destination_address_lat'] = BaseFieldDefinition::create('decimal')
      ->setLabel(t('Latitude'))
      ->setDefaultValue(48.8955888)
      ->setSetting('precision', 12)
      ->setSetting('scale', 8)
      ->setSetting('min', -90)
      ->setSetting('max', 90)
      //->setSetting('step', '0.00000001')
      //->setSetting('step', 'any')
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => 4,
        'settings' => [
          'placeholder' => '0.00',
        ],
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'number_decimal',
        'weight' => 4,
        'settings' => [
          'thousand_separator' => '',
          'decimal_separator' => '.',
          'precision' => 12,
          'scale' => 8,
        ],
      ])
      ->setCardinality(1);

    $fields['map_settings_destination_address_lng'] = BaseFieldDefinition::create('decimal')
      ->setLabel(t('Longitude'))
      ->setDefaultValue(2.3821658)
      ->setSetting('precision', 12)
      ->setSetting('scale', 8)
      ->setSetting('min', -180)
      ->setSetting('max', 180)
      //->setSetting('step', '0.00000001')
      //->setSetting('step', 'any')
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => 5,
        'settings' => [
          'placeholder' => '0.00',
        ],
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'number_decimal',
        'weight' => 5,
        'settings' => [
          'thousand_separator' => '',
          'decimal_separator' => '.',
          'precision' => 12,
          'scale' => 8,
        ],
      ])
      ->setCardinality(1);

    $fields['map_settings_destination_address_enable_direction'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Enable direction'))
      ->setDescription(t('A boolean indicating whether the map route has direction.'))
      ->setDefaultValue(FALSE)
      ->setSetting('on_label', 'Enable')
      ->setSetting('off_label', 'Disable')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => TRUE,
        ],
        'weight' => 6,
      ])
      ->setDisplayOptions('view', [
        'type' => 'boolean',
        'label' => 'hidden',
        'weight' => 6,
        'settings' => [
          'format' => 'enabled-disabled',
        ],
      ]);

    // Settings map zoom.
    $min_zoom = 0;
    $max_zoom = 21;
    $fields['map_settings_map_zoom_default_zoom'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Default zoom'))
      ->setDefaultValue(16)
      ->setSetting('unsigned', FALSE)
      ->setSetting('size', 'normal')
      ->setSetting('min', $min_zoom)
      ->setSetting('max', $max_zoom)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'number_integer',
        'weight' => 7,
      ])
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => 7,
      ])
      ->setCardinality(1)
      ->setRequired(TRUE);

    $fields['map_settings_map_zoom_zoom_control'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Enable zoom control'))
      ->setDescription(t('A boolean indicating whether the map route has zoom control.'))
      ->setDefaultValue(FALSE)
      ->setSetting('on_label', 'Enable')
      ->setSetting('off_label', 'Disable')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => TRUE,
        ],
        'weight' => 8,
      ])
      ->setDisplayOptions('view', [
        'type' => 'boolean',
        'label' => 'hidden',
        'weight' => 8,
        'settings' => [
          'format' => 'enabled-disabled',
        ],
      ]);

    $fields['map_settings_map_zoom_zoom_scroll_wheel'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Enable zoom scroll wheel'))
      ->setDescription(t('A boolean indicating whether the map route is zoom scrollable.'))
      ->setDefaultValue(FALSE)
      ->setSetting('on_label', 'Enable')
      ->setSetting('off_label', 'Disable')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => TRUE,
        ],
        'weight' => 9,
      ])
      ->setDisplayOptions('view', [
        'type' => 'boolean',
        'label' => 'hidden',
        'weight' => 9,
        'settings' => [
          'format' => 'enabled-disabled',
        ],
      ]);

    $fields['map_settings_map_zoom_min_zoom'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Minimum zoom authorized'))
      ->setDefaultValue($min_zoom)
      ->setSetting('unsigned', FALSE)
      ->setSetting('size', 'normal')
      ->setSetting('min', $min_zoom)
      ->setSetting('max', $max_zoom)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'number_integer',
        'weight' => 10,
      ])
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => 10,
      ])
      ->setCardinality(1)
      ->setRequired(TRUE);

    $fields['map_settings_map_zoom_max_zoom'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Maximum zoom authorized'))
      ->setDefaultValue($max_zoom)
      ->setSetting('unsigned', FALSE)
      ->setSetting('size', 'normal')
      ->setSetting('min', $min_zoom)
      ->setSetting('max', $max_zoom)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'number_integer',
        'weight' => 11,
      ])
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => 11,
      ])
      ->setCardinality(1)
      ->setRequired(TRUE);

    // Settings map_type.
    $map_types = [
      'roadmap' => t('RoadMap'),
      'satellite' => t('Satellite'),
      'hybrid' => t('Hybrid'),
      'terrain' => t('Terrain'),
    ];
    $fields['map_settings_map_type_map_type_default'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Map type'))
      ->setDefaultValue('terrain')
      ->setSettings([
        'allowed_values' => $map_types,
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 12,
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'list_default',
        'weight' => 12,
      ])
      ->setCardinality(1)
      ->setRequired(TRUE);

    $fields['map_settings_map_type_map_type_control'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Enable map type control'))
      ->setDescription(t('A boolean indicating whether the map route has the map type control.'))
      ->setDefaultValue(FALSE)
      ->setSetting('on_label', 'Enable')
      ->setSetting('off_label', 'Disable')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => TRUE,
        ],
        'weight' => 13,
      ])
      ->setDisplayOptions('view', [
        'type' => 'boolean',
        'label' => 'hidden',
        'weight' => 13,
        'settings' => [
          'format' => 'enabled-disabled',
        ],
      ]);

    $fields['map_settings_map_type_map_type_control_options'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Map type'))
      ->setDefaultValue([
        'roadmap',
        'satellite',
        'hybrid',
        'terrain',
      ])
      ->setSettings([
        'allowed_values' => $map_types,
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 14,
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'list_default',
        'weight' => 14,
      ])
      ->setCardinality(-1)
      ->setRequired(TRUE);

    // Other map controls.
    $fields['map_other_map_controls_scale_control'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Enable scale control'))
      ->setDescription(t('A boolean indicating whether the map route has the scale control.'))
      ->setDefaultValue(TRUE)
      ->setSetting('on_label', 'Enable')
      ->setSetting('off_label', 'Disable')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => TRUE,
        ],
        'weight' => 15,
      ])
      ->setDisplayOptions('view', [
        'type' => 'boolean',
        'label' => 'hidden',
        'weight' => 15,
        'settings' => [
          'format' => 'enabled-disabled',
        ],
      ]);

    $fields['map_other_map_controls_fullscreen_control'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Enable fullscreen control'))
      ->setDescription(t('A boolean indicating whether the map route has the fullscreen control.'))
      ->setDefaultValue(TRUE)
      ->setSetting('on_label', 'Enable')
      ->setSetting('off_label', 'Disable')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => TRUE,
        ],
        'weight' => 16,
      ])
      ->setDisplayOptions('view', [
        'type' => 'boolean',
        'label' => 'hidden',
        'weight' => 16,
        'settings' => [
          'format' => 'enabled-disabled',
        ],
      ]);

    $fields['map_other_map_controls_street_view_control'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Enable street view control'))
      ->setDescription(t('A boolean indicating whether the map route has the street view control.'))
      ->setDefaultValue(FALSE)
      ->setSetting('on_label', 'Enable')
      ->setSetting('off_label', 'Disable')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => TRUE,
        ],
        'weight' => 17,
      ])
      ->setDisplayOptions('view', [
        'type' => 'boolean',
        'label' => 'hidden',
        'weight' => 17,
        'settings' => [
          'format' => 'enabled-disabled',
        ],
      ]);

    // Settings marker.
    $fields['map_settings_marker_enable_marker'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Enable default marker'))
      ->setDescription(t('A boolean indicating whether the map route has the street view control.'))
      ->setDefaultValue(TRUE)
      ->setSetting('on_label', 'Enable')
      ->setSetting('off_label', 'Disable')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => TRUE,
        ],
        'weight' => 18,
      ])
      ->setDisplayOptions('view', [
        'type' => 'boolean',
        'label' => 'hidden',
        'weight' => 18,
        'settings' => [
          'format' => 'enabled-disabled',
        ],
      ]);

    $fields['map_settings_marker_opacity'] = BaseFieldDefinition::create('decimal')
      ->setLabel(t('Opacity'))
      ->setDefaultValue(0.75)
      ->setSetting('precision', 4)
      ->setSetting('scale', 2)
      ->setSetting('min', 0)
      ->setSetting('max', 1)
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => 19,
        'settings' => [
          'placeholder' => '0.00',
        ],
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'number_decimal',
        'weight' => 19,
        'settings' => [
          'thousand_separator' => '',
          'decimal_separator' => '.',
          'precision' => 12,
          'scale' => 8,
        ],
      ])
      ->setCardinality(1);

    $fields['map_settings_marker_override_default_svg'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Override default marker svg'))
      ->setDefaultValue(FALSE)
      ->setSetting('on_label', 'Override')
      ->setSetting('off_label', 'Don\'t override')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => TRUE,
        ],
        'weight' => 20,
      ])
      ->setDisplayOptions('view', [
        'type' => 'boolean',
        'label' => 'hidden',
        'weight' => 20,
        'settings' => [
          'format' => 'enabled-disabled',
        ],
      ]);

    $fields['map_settings_marker_enable_title'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Enable title on the marker'))
      ->setDefaultValue(FALSE)
      ->setSetting('on_label', 'Enable')
      ->setSetting('off_label', 'Disable')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => TRUE,
        ],
        'weight' => 22,
      ])
      ->setDisplayOptions('view', [
        'type' => 'boolean',
        'label' => 'hidden',
        'weight' => 22,
        'settings' => [
          'format' => 'enabled-disabled',
        ],
      ]);

    $fields['map_settings_marker_override_default_style_title'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Override default style for the marker title'))
      ->setDefaultValue(FALSE)
      ->setSetting('on_label', 'Override')
      ->setSetting('off_label', 'Don\'t override')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => TRUE,
        ],
        'weight' => 23,
      ])
      ->setDisplayOptions('view', [
        'type' => 'boolean',
        'label' => 'hidden',
        'weight' => 23,
        'settings' => [
          'format' => 'enabled-disabled',
        ],
      ]);

    $fields['map_settings_marker_style_title_font_size'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Override the font-size style for the marker title'))
      ->setSetting('max_length', 100)
      ->setCardinality(1)
      ->setDefaultValue('1rem')
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 24,
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => 24,
      ]);

    $fields['map_settings_marker_style_title_font_weight'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Override the font-weight style for the marker title'))
      ->setSetting('max_length', 100)
      ->setCardinality(1)
      ->setDefaultValue('400')
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 25,
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => 25,
      ]);

    $fields['map_settings_marker_style_title_color'] = BaseFieldDefinition::create('color_field_type')
      ->setLabel(t('Override the color style for the marker title'))
      ->setDefaultValue([
        'color' => '#DB4437',
        'opacity' => 1,
      ])
      ->setSetting('opacity', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'color_field_widget_html5',
        'weight' => 26,
        'settings' => [
          'size' => 5,
        ],
      ])
      ->setDisplayOptions('view', [
        'type' => 'color_field_formatter_text',
        'label' => 'hidden',
        'weight' => 26,
      ])
      ->setRequired(TRUE)
      ->setCardinality(1);

    $fields['map_settings_marker_enable_window_description'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Enable window description on the marker'))
      ->setDefaultValue(TRUE)
      ->setSetting('on_label', 'Enable')
      ->setSetting('off_label', 'Disable')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => TRUE,
        ],
        'weight' => 27,
      ])
      ->setDisplayOptions('view', [
        'type' => 'boolean',
        'label' => 'hidden',
        'weight' => 27,
        'settings' => [
          'format' => 'enabled-disabled',
        ],
      ]);

  }

}
