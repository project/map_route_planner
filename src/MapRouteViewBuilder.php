<?php

namespace Drupal\map_route_planner;

use Drupal\map_route_planner\Factory\MapRouteFactory;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityViewBuilder;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Theme\Registry;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * View builder handler for comments.
 */
class MapRouteViewBuilder extends EntityViewBuilder {

  /**
   * The Map Route factory.
   *
   * @var MapRouteFactory
   */
  protected $mapRouteFactory;

  /**
   * Constructs a new CommentViewBuilder.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Theme\Registry $theme_registry
   *   The theme registry.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   *   The entity display repository.
   * @param MapRouteFactory $map_route_factory
   *   The Map Route factory.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityRepositoryInterface $entity_repository, LanguageManagerInterface $language_manager, Registry $theme_registry, EntityDisplayRepositoryInterface $entity_display_repository, MapRouteFactory $map_route_factory) {
    parent::__construct($entity_type, $entity_repository, $language_manager, $theme_registry, $entity_display_repository);
    $this->mapRouteFactory = $map_route_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity.repository'),
      $container->get('language_manager'),
      $container->get('theme.registry'),
      $container->get('entity_display.repository'),
			$container->get('map_route.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getBuildDefaults(EntityInterface $entity, $view_mode) {
    $build = parent::getBuildDefaults($entity, $view_mode);

    /** @var \Drupal\map_route_planner\MapRouteInterface $entity */

    return $build;
  }

  /**
   * {@inheritdoc}
   *
   * In addition to modifying the content key on entities, this implementation
   * will also set the comment entity key which all comments carry.
   *
   * @throws \InvalidArgumentException
   *   Thrown when a comment is attached to an entity that no longer exists.
   */
  public function buildComponents(array &$build, array $entities, array $displays, $view_mode) {
    /** @var \Drupal\map_route_planner\MapRouteInterface[] $entities */
    if (empty($entities)) {
      return;
    }
    // parent::buildComponents($build, $entities, $displays, $view_mode);

    foreach ($entities as $id => $entity) {
      $bundle = $entity ? $entity->bundle() : '';
      $entity_type_id = $entity->getEntityTypeId();

      $build[$id]['#entity'] = $entity;
      //$build[$id]['#theme'] = $entity_type_id. '__' . $bundle . '__' . $view_mode;

      if (!isset($build[$id]['#attached'])) {
        $build[$id]['#attached'] = [];
      }

      $build[$id]['#attached']['drupalSettings']['map_route'] = $this->mapRouteFactory
        ->getFieldsValueByMapRoute($entity);
      $build[$id]['#attached']['library'][] = 'map_route_planner/google_map';

      $build[$id]['#attached']['drupalSettings']['map_route']['appearance'] = $this->mapRouteFactory
        ->getFieldsValueByMapRoute($entity, ['appearance']);
      $build[$id]['#attached']['library'][] = 'map_route_planner/appearance';

      if ($entity->get('map_settings_map_enable_geoloc')->value) {
        $build[$id]['#attached']['library'][] = 'map_route_planner/form_geolocation_client_address';
      }
      if ($entity->get('form_enable_element_btn_switch')->value) {
        $build[$id]['#attached']['library'][] = 'map_route_planner/form_addresses_switch';
      }
      if ($entity->get('form_enable_element_btn_minimize_restore')->value) {
        $build[$id]['#attached']['library'][] = 'map_route_planner/form_minimization';
      }
      $build[$id]['map_route'] = $this->mapRouteFactory
        ->getFieldsValueByMapRoute($entity, ['form']);
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function alterBuild(array &$build, EntityInterface $entity, EntityViewDisplayInterface $display, $view_mode) {
    parent::alterBuild($build, $entity, $display, $view_mode);
  }

}
