<?php

namespace Drupal\map_route_planner\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\map_route_planner\Factory\GoogleMapsFactory;
use Drupal\map_route_planner\Factory\MapRouteFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for the map route entity edit forms.
 */
class MapRouteForm extends ContentEntityForm {
  /**
   * The Google Maps Factory.
   *
   * @var \Drupal\map_route_planner\Factory\GoogleMapsFactory
   */
  protected $googleMapsFactory;

  /**
   * The Map Route factory.
   *
   * @var \Drupal\map_route_planner\Factory\MapRouteFactory
   */
  protected $mapRouteFactory;

  /**
   * Constructs a MapRoute Form.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\map_route_planner\Factory\GoogleMapsFactory $google_maps_factory
   *   The Google Maps Factory.
   * @param MapRouteFactory $map_route_factory
   *   The Map Route factory.
   */
  public function __construct(
    EntityRepositoryInterface $entity_repository,
    EntityTypeBundleInfoInterface $entity_type_bundle_info,
    TimeInterface $time,
    GoogleMapsFactory $google_maps_factory,
    MapRouteFactory $map_route_factory
  ) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);
    $this->googleMapsFactory = $google_maps_factory;
    $this->mapRouteFactory = $map_route_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('google_maps.factory'),
      $container->get('map_route.factory')
    );
  }
  /**
   * {@inheritdoc}
   */

  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    // Create main vertical_tabs who's named "map_route".
    $form['map_route'] = [
      '#type' => 'horizontal_tabs',
      '#default_tab' => 'edit-map',
      '#weight' => -4,
    ];

    // Map tab.
    $form['map'] = [
      '#type' => 'details',
      '#title' => $this->t('Map'),
      '#group' => 'map_route',
    ];

    // Map tab - sub vertical_tabs who's named "map_vertical_tabs".
    $form['map']['map_vertical_tabs'] = [
      '#type' => 'vertical_tabs',
      '#default_tab' => 'edit-settings-destination-address',
    ];

    // Map tab - Settings destination address sub-tab.
    $form['map']['settings_destination_address'] = [
      '#type' => 'details',
      '#title' => $this->t('Settings destination address'),
      '#group' => 'map_vertical_tabs',
    ];

    // Map tab - Settings destination address sub-tab - Rewrite some fields.
    if (!empty($form['map_settings_destination_address_title'])) {
      $form['map']['settings_destination_address']['title'] = $form['map_settings_destination_address_title'];
      unset($form['map_settings_destination_address_title']);
    }

    $states_enable_marker_and_description = [
      'visible' => [
        'input[name^="map_settings_marker_enable_marker"]' => ['checked' => TRUE],
        'input[name^="map_settings_marker_enable_window_description"]' => ['checked' => TRUE],
      ],
    ];
    if (!empty($form['map_settings_destination_address_description'])) {
      $form['map']['settings_destination_address']['description'] = $form['map_settings_destination_address_description'];
      unset($form['map_settings_destination_address_description']);
      if (!empty($states_enable_marker_and_description)) {
        $form['map']['settings_destination_address']['description']['#states'] = $states_enable_marker_and_description;
      }
    }
    $states_location_type_address = [];
    $states_location_type_lat_lng = [];
    if (!empty($form['map_settings_destination_address_location_type'])) {
      $form['map']['settings_destination_address']['location_type'] = $form['map_settings_destination_address_location_type'];
      unset($form['map_settings_destination_address_location_type']);
      $states_location_type_address = [
        'visible' => [
          ['select[name="map_settings_destination_address_location_type"]' => [
            'value' => 'address',
          ]],
        ],
      ];
      $states_location_type_lat_lng = [
        'visible' => [
          ['select[name="map_settings_destination_address_location_type"]' => [
            'value' => 'lat_lng',
          ]],
        ],
      ];
    }
    if (!empty($form['map_settings_destination_address_address'])) {
      $form['map']['settings_destination_address']['address'] = $form['map_settings_destination_address_address'];
      unset($form['map_settings_destination_address_address']);
      if (!empty($states_location_type_address)) {
        $form['map']['settings_destination_address']['address']['#states'] = $states_location_type_address;
      }
    }
    if (!empty($form['map_settings_destination_address_lat'])) {
      $form['map']['settings_destination_address']['lat'] = $form['map_settings_destination_address_lat'];
      unset($form['map_settings_destination_address_lat']);
      if (!empty($states_location_type_lat_lng)) {
        $form['map']['settings_destination_address']['lat']['#states'] = $states_location_type_lat_lng;
      }
    }
    if (!empty($form['map_settings_destination_address_lng'])) {
      $form['map']['settings_destination_address']['lng'] = $form['map_settings_destination_address_lng'];
      unset($form['map_settings_destination_address_lng']);
      if (!empty($states_location_type_lat_lng)) {
        $form['map']['settings_destination_address']['lng']['#states'] = $states_location_type_lat_lng;
      }
    }
    if (!empty($form['map_settings_destination_address_enable_direction'])) {
      $form['map']['settings_destination_address']['enable_direction'] = $form['map_settings_destination_address_enable_direction'];
      unset($form['map_settings_destination_address_enable_direction']);
      // @todo : Probably many states to create with this boolean in the full form and many parts of appearance.
      // @todo : Make this after all fields were converted.
    }

    // Map tab - Settings map zoom sub-tab.
    $form['map']['settings_map_zoom'] = [
      '#type' => 'details',
      '#title' => $this->t('Settings map zoom'),
      '#group' => 'map_vertical_tabs',
    ];

    // Map tab - Settings map zoom sub-tab - Rewrite some fields.
    if (!empty($form['map_settings_map_zoom_default_zoom'])) {
      $form['map']['settings_map_zoom']['default_zoom'] = $form['map_settings_map_zoom_default_zoom'];
      unset($form['map_settings_map_zoom_default_zoom']);
    }
    $states_zoom_control = [];
    if (!empty($form['map_settings_map_zoom_zoom_control'])) {
      $form['map']['settings_map_zoom']['zoom_control'] = $form['map_settings_map_zoom_zoom_control'];
      unset($form['map_settings_map_zoom_zoom_control']);
      $states_zoom_control = [
        'visible' => [
          ['input[name^="map_settings_map_zoom_zoom_control"]' => ['checked' => TRUE]],
          ['input[name^="map_settings_map_zoom_zoom_scroll_wheel"]' => ['checked' => TRUE]],
        ],
      ];
    }
    if (!empty($form['map_settings_map_zoom_zoom_scroll_wheel'])) {
      $form['map']['settings_map_zoom']['zoom_scroll_wheel'] = $form['map_settings_map_zoom_zoom_scroll_wheel'];
      unset($form['map_settings_map_zoom_zoom_scroll_wheel']);
    }
    if (!empty($form['map_settings_map_zoom_min_zoom'])) {
      $form['map']['settings_map_zoom']['min_zoom'] = $form['map_settings_map_zoom_min_zoom'];
      unset($form['map_settings_map_zoom_min_zoom']);
      if (!empty($states_zoom_control)) {
        $form['map']['settings_map_zoom']['min_zoom']['#states'] = $states_zoom_control;
      }
    }
    if (!empty($form['map_settings_map_zoom_max_zoom'])) {
      $form['map']['settings_map_zoom']['max_zoom'] = $form['map_settings_map_zoom_max_zoom'];
      unset($form['map_settings_map_zoom_max_zoom']);
      if (!empty($states_zoom_control)) {
        $form['map']['settings_map_zoom']['max_zoom']['#states'] = $states_zoom_control;
      }
    }

    // Map tab - Settings map type sub-tab.
    $form['map']['settings_map_type'] = [
      '#type' => 'details',
      '#title' => $this->t('Settings map type'),
      '#group' => 'map_vertical_tabs',
    ];

    // Map tab - Settings map type sub-tab - Rewrite some fields.
    if (!empty($form['map_settings_map_type_map_type_default'])) {
      $form['map']['settings_map_type']['map_type_default'] = $form['map_settings_map_type_map_type_default'];
      unset($form['map_settings_map_type_map_type_default']);
    }
    $states_map_type_control = [];
    if (!empty($form['map_settings_map_type_map_type_control'])) {
      $form['map']['settings_map_type']['map_type_control'] = $form['map_settings_map_type_map_type_control'];
      unset($form['map_settings_map_type_map_type_control']);

      $states_map_type_control = [
        'visible' => [
          ['input[name^="map_settings_map_type_map_type_control"]' => ['checked' => TRUE]],
        ],
      ];
    }
    if (!empty($form['map_settings_map_type_map_type_control_options'])) {
      $form['map']['settings_map_type']['map_type_control_options'] = $form['map_settings_map_type_map_type_control_options'];
      unset($form['map_settings_map_type_map_type_control_options']);
      $form['map']['settings_map_type']['map_type_control_options']['#states'] = $states_map_type_control;
    }

    // Map tab - Other map controls sub-tab.
    $form['map']['other_map_controls'] = [
      '#type' => 'details',
      '#title' => $this->t('Other map controls'),
      '#group' => 'map_vertical_tabs',
    ];

    // Map tab - Other map controls sub-tab - Rewrite some fields.
    if (!empty($form['map_other_map_controls_scale_control'])) {
      $form['map']['other_map_controls']['scale_control'] = $form['map_other_map_controls_scale_control'];
      unset($form['map_other_map_controls_scale_control']);
    }
    if (!empty($form['map_other_map_controls_fullscreen_control'])) {
      $form['map']['other_map_controls']['fullscreen_control'] = $form['map_other_map_controls_fullscreen_control'];
      unset($form['map_other_map_controls_fullscreen_control']);
    }
    if (!empty($form['map_other_map_controls_street_view_control'])) {
      $form['map']['other_map_controls']['street_view_control'] = $form['map_other_map_controls_street_view_control'];
      unset($form['map_other_map_controls_street_view_control']);
    }

    // Map tab - Settings marker sub-tab.
    $form['map']['settings_marker'] = [
      '#type' => 'details',
      '#title' => $this->t('Settings marker'),
      '#group' => 'map_vertical_tabs',
    ];

    // Map tab - Settings marker sub-tab - Rewrite some fields.
    $states_enable_marker = [];
    if (!empty($form['map_settings_marker_enable_marker'])) {
      $form['map']['settings_marker']['enable_marker'] = $form['map_settings_marker_enable_marker'];
      unset($form['map_settings_marker_enable_marker']);

      $states_enable_marker = [
        'visible' => [
          'input[name^="map_settings_marker_enable_marker"]' => ['checked' => TRUE],
        ],
      ];
    }
    if (!empty($form['map_settings_marker_opacity'])) {
      $form['map']['settings_marker']['opacity'] = $form['map_settings_marker_opacity'];
      unset($form['map_settings_marker_opacity']);
      if (!empty($states_enable_marker)) {
        $form['map']['settings_marker']['opacity']['#states'] = $states_enable_marker;
      }
    }
    if (!empty($form['map_settings_marker_override_default_svg'])) {
      $form['map']['settings_marker']['override_default_svg'] = $form['map_settings_marker_override_default_svg'];
      unset($form['map_settings_marker_override_default_svg']);
      if (!empty($states_enable_marker)) {
        $form['map']['settings_marker']['override_default_svg']['#states'] = $states_enable_marker;
      }
    }
    /*if (!empty($form['map_settings_marker_new_marker_svg'])) {
      $form['map']['settings_marker']['new_marker_svg'] = $form['map_settings_marker_new_marker_svg'];
      unset($form['map_settings_marker_new_marker_svg']);
      // @todo : create the state after create the field.
    }*/

    $states_enable_marker_and_title = [];
    if (!empty($form['map_settings_marker_enable_title'])) {
      $form['map']['settings_marker']['enable_title'] = $form['map_settings_marker_enable_title'];
      unset($form['map_settings_marker_enable_title']);
      if (!empty($states_enable_marker)) {
        $form['map']['settings_marker']['enable_title']['#states'] = $states_enable_marker;
        $states_enable_marker_and_title = $states_enable_marker;
        $states_enable_marker_and_title['visible']['input[name^="map_settings_marker_enable_title"]'] = ['checked' => TRUE];
      }
    }
    $states_enable_marker_and_title_override = [];
    if (!empty($form['map_settings_marker_override_default_style_title'])) {
      $form['map']['settings_marker']['override_default_style_title'] = $form['map_settings_marker_override_default_style_title'];
      unset($form['map_settings_marker_override_default_style_title']);
      if (!empty($states_enable_marker_and_title)) {
        $form['map']['settings_marker']['override_default_style_title']['#states'] = $states_enable_marker_and_title;
        $states_enable_marker_and_title_override = $states_enable_marker_and_title;
        $states_enable_marker_and_title_override['visible']['input[name^="map_settings_marker_override_default_style_title"]'] = ['checked' => TRUE];
      }
    }
    if (!empty($form['map_settings_marker_style_title_font_size'])) {
      $form['map']['settings_marker']['style_title_font_size'] = $form['map_settings_marker_style_title_font_size'];
      unset($form['map_settings_marker_style_title_font_size']);
      if (!empty($states_enable_marker_and_title_override)) {
        $form['map']['settings_marker']['style_title_font_size']['#states'] = $states_enable_marker_and_title_override;
      }
    }
    if (!empty($form['map_settings_marker_style_title_font_weight'])) {
      $form['map']['settings_marker']['style_title_font_weight'] = $form['map_settings_marker_style_title_font_weight'];
      unset($form['map_settings_marker_style_title_font_weight']);
      if (!empty($states_enable_marker_and_title_override)) {
        $form['map']['settings_marker']['style_title_font_weight']['#states'] = $states_enable_marker_and_title_override;
      }
    }
    if (!empty($form['map_settings_marker_style_title_color'])) {
      $form['map']['settings_marker']['style_title_color'] = $form['map_settings_marker_style_title_color'];
      unset($form['map_settings_marker_style_title_color']);
      if (!empty($states_enable_marker_and_title_override)) {
        $form['map']['settings_marker']['style_title_color']['#states'] = $states_enable_marker_and_title_override;
      }
    }
    if (!empty($form['map_settings_marker_enable_window_description'])) {
      $form['map']['settings_marker']['enable_window_description'] = $form['map_settings_marker_enable_window_description'];
      unset($form['map_settings_marker_enable_window_description']);
      if (!empty($states_enable_marker)) {
        $form['map']['settings_marker']['enable_window_description']['#states'] = $states_enable_marker;
      }
    }
    // @todo
    /*
    if (!empty($form['map_map_center_address_or_lat_lng'])) {
      $form['map']['map_center']['address_or_lat_lng'] = $form['map_map_center_address_or_lat_lng'];
      unset($form['map_map_center_address_or_lat_lng']);
      if (!empty($form['map_map_center_address'])) {
        $form['map']['map_center']['address'] = $form['map_map_center_address'];
        unset($form['map_map_center_address']);
        $form['map']['map_center']['address']['#states'] = [
          'visible' => [
            'select[name="map_map_center_address_or_lat_lng"]' => [
              'value' => 'address',
            ],
          ],
        ];
      }
      if (!empty($form['map_map_center_lat'])) {
        $form['map']['map_center']['lat'] = $form['map_map_center_lat'];
        unset($form['map_map_center_lat']);
        $form['map']['map_center']['lat']['#states'] = [
          'visible' => [
            'select[name="map_map_center_address_or_lat_lng"]' => [
              'value' => 'lat_lng',
            ],
          ],
        ];
      }
      if (!empty($form['map_map_center_lng'])) {
        $form['map']['map_center']['lng'] = $form['map_map_center_lng'];
        unset($form['map_map_center_lng']);
        $form['map']['map_center']['lng']['#states'] = [
          'visible' => [
            'select[name="map_map_center_address_or_lat_lng"]' => [
              'value' => 'lat_lng',
            ],
          ],
        ];
      }
    }

    // Map tab - Settings Map sub-tab.
    $form['map']['settings_map'] = [
      '#type' => 'details',
      '#title' => $this->t('Map settings'),
      '#group' => 'map_vertical_tabs',
    ];

    // Map tab - Settings Map sub-tab - Rewrite some fields.
    if (!empty($form['map_settings_map_enable_geoloc'])) {
      $form['map']['settings_map']['enable_geoloc'] = $form['map_settings_map_enable_geoloc'];
      unset($form['map_settings_map_enable_geoloc']);
    }
    if (!empty($form['map_settings_map_map_type'])) {
      $form['map']['settings_map']['map_type'] = $form['map_settings_map_map_type'];
      unset($form['map_settings_map_map_type']);
    }
    if (!empty($form['map_settings_map_zoom_scroll'])) {
      $form['map']['settings_map']['zoom_scroll'] = $form['map_settings_map_zoom_scroll'];
      unset($form['map_settings_map_zoom_scroll']);
    }
    $max = 21;
    if (!empty($form['map_settings_map_zoom_max'])) {
      if (!empty($form['map_settings_map_zoom_max']['widget'][0]['value']['#default_value'])) {
        $max = $form['map_settings_map_zoom_max']['widget'][0]['value']['#default_value'];
      }
      $form['map']['settings_map']['zoom_max'] = $form['map_settings_map_zoom_max'];
      unset($form['map_settings_map_zoom_max']);
    }
    if (!empty($form['map_settings_map_zoom'])) {
      $form['map_settings_map_zoom']['widget'][0]['value']['#max'] = $max;
      $form['map']['settings_map']['zoom'] = $form['map_settings_map_zoom'];
      unset($form['map_settings_map_zoom']);
    }

    // Marker Tab.
    $form['marker'] = [
      '#type' => 'details',
      '#title' => $this->t('Marker'),
      '#group' => 'map_route',
    ];

    // Marker tab - sub vertical_tabs who's named "marker_vertical_tabs".
    $form['marker']['marker_vertical_tabs'] = [
      '#type' => 'vertical_tabs',
      '#default_tab' => 'edit-marker-position',
    ];

    // Marker tab - Marker Position sub-tab.
    $form['marker']['marker_position'] = [
      '#type' => 'details',
      '#title' => $this->t('Default Marker Position'),
      '#group' => 'marker_vertical_tabs',
    ];

    // Marker tab - Marker Position sub-tab - Rewrite some fields.
    if (!empty($form['marker_marker_position_address_or_lat_lng'])) {
      $form['marker']['marker_position']['address_or_lat_lng'] = $form['marker_marker_position_address_or_lat_lng'];
      unset($form['marker_marker_position_address_or_lat_lng']);
      if (!empty($form['marker_marker_position_address'])) {
        $form['marker']['marker_position']['address'] = $form['marker_marker_position_address'];
        unset($form['marker_marker_position_address']);
        $form['marker']['marker_position']['address']['#states'] = [
          'visible' => [
            'select[name="marker_marker_position_address_or_lat_lng"]' => [
              'value' => 'address',
            ],
          ],
        ];
      }
      if (!empty($form['marker_marker_position_lat'])) {
        $form['marker']['marker_position']['lat'] = $form['marker_marker_position_lat'];
        unset($form['marker_marker_position_lat']);
        $form['marker']['marker_position']['lat']['#states'] = [
          'visible' => [
            'select[name="marker_marker_position_address_or_lat_lng"]' => [
              'value' => 'lat_lng',
            ],
          ],
        ];
      }
      if (!empty($form['marker_marker_position_lng'])) {
        $form['marker']['marker_position']['lng'] = $form['marker_marker_position_lng'];
        unset($form['marker_marker_position_lng']);
        $form['marker']['marker_position']['lng']['#states'] = [
          'visible' => [
            'select[name="marker_marker_position_address_or_lat_lng"]' => [
              'value' => 'lat_lng',
            ],
          ],
        ];
      }
    }

    // Marker tab - Info sub-tab.
    $form['marker']['info'] = [
      '#type' => 'details',
      '#title' => $this->t('Default Text'),
      '#group' => 'marker_vertical_tabs',
    ];

    // Marker tab - Info sub-tab - Rewrite some fields.
    if (!empty($form['marker_info_title'])) {
      $form['marker']['info']['title'] = $form['marker_info_title'];
      unset($form['marker_info_title']);
    }
    if (!empty($form['marker_info_enable_window_description'])) {
      $form['marker']['info']['enable_window_description'] = $form['marker_info_enable_window_description'];
      unset($form['marker_info_enable_window_description']);
      if (!empty($form['marker_info_window_description'])) {
        $form['marker']['info']['window_description'] = $form['marker_info_window_description'];
        unset($form['marker_info_window_description']);
        $form['marker']['info']['window_description'] ['#states'] = [
          'visible' => [
            'input[name^="marker_info_enable_window_description"]' => ['checked' => TRUE],
          ],
        ];
      }
    }
    */
    // @todo

    // Form Tab.
    $form['form'] = [
      '#type' => 'details',
      '#title' => $this->t('Form'),
      '#group' => 'map_route',
    ];

    // Form tab - sub vertical_tabs who's named "form_vertical_tabs".
    $form['form']['form_vertical_tabs'] = [
      '#type' => 'vertical_tabs',
      '#default_tab' => 'edit-destination',
    ];

    // Form tab - Destination sub-tab.
    $form['form']['destination'] = [
      '#type' => 'details',
      '#title' => $this->t('Destination address'),
      '#group' => 'form_vertical_tabs',
    ];

    // Form tab - Destination sub-tab - Rewrite some fields.
    if (!empty($form['form_destination_title'])) {
      $form['form']['destination']['title'] = $form['form_destination_title'];
      unset($form['form_destination_title']);
    }
    if (!empty($form['form_destination_address'])) {
      $form['form']['destination']['address'] = $form['form_destination_address'];
      unset($form['form_destination_address']);
    }

    // Form tab - Enable Element sub-tab.
    $form['form']['enable_element'] = [
      '#type' => 'details',
      '#title' => $this->t('Enable element'),
      '#group' => 'form_vertical_tabs',
    ];

    // Form tab - Enable Element sub-tab - Rewrite some fields.
    if (!empty($form['form_enable_element_transport'])) {
      $form['form']['enable_element']['transport'] = $form['form_enable_element_transport'];
      unset($form['form_enable_element_transport']);
    }
    if (!empty($form['form_enable_element_btn_switch'])) {
      $form['form']['enable_element']['btn_switch'] = $form['form_enable_element_btn_switch'];
      unset($form['form_enable_element_btn_switch']);
    }
    if (!empty($form['form_enable_element_btn_minimize_restore'])) {
      $form['form']['enable_element']['btn_minimize_restore'] = $form['form_enable_element_btn_minimize_restore'];
      unset($form['form_enable_element_btn_minimize_restore']);
    }

    // Form tab - Label Address sub-tab.
    $form['form']['label_address'] = [
      '#type' => 'details',
      '#title' => $this->t('Label address'),
      '#group' => 'form_vertical_tabs',
    ];

    // Form tab - Label Address sub-tab - Rewrite some fields.
    if (!empty($form['form_label_address_show_start'])) {
      $form['form']['label_address']['show_start'] = $form['form_label_address_show_start'];
      unset($form['form_label_address_show_start']);
      if (!empty($form['form_label_address_text_start'])) {
        $form['form']['label_address']['text_start'] = $form['form_label_address_text_start'];
        unset($form['form_label_address_text_start']);
        $form['form']['label_address']['text_start']['#states'] = [
          'visible' => [
            'input[name^="form_label_address_show_start"]' => ['checked' => TRUE],
          ],
        ];
      }
    }
    if (!empty($form['form_label_address_show_end'])) {
      $form['form']['label_address']['show_end'] = $form['form_label_address_show_end'];
      unset($form['form_label_address_show_end']);
      if (!empty($form['form_label_address_text_end'])) {
        $form['form']['label_address']['text_end'] = $form['form_label_address_text_end'];
        unset($form['form_label_address_text_end']);
        $form['form']['label_address']['text_end']['#states'] = [
          'visible' => [
            'input[name^="form_label_address_show_end"]' => ['checked' => TRUE],
          ],
        ];
      }
    }

    // Form tab - Other Options sub-tab.
    $form['form']['other_options'] = [
      '#type' => 'details',
      '#title' => $this->t('Other options'),
      '#group' => 'form_vertical_tabs',
    ];

    // Form tab - Other Options sub-tab - Rewrite some fields.
    if (!empty($form['form_other_options_starting_address_placeholder'])) {
      $form['form']['other_options']['starting_address_placeholder'] = $form['form_other_options_starting_address_placeholder'];
      unset($form['form_other_options_starting_address_placeholder']);
    }
    if (!empty($form['form_other_options_submit_button_label'])) {
      $form['form']['other_options']['submit_button_label'] = $form['form_other_options_submit_button_label'];
      unset($form['form_other_options_submit_button_label']);
    }

    // Appearence Tab.
    $form['appearance'] = [
      '#type' => 'details',
      '#title' => $this->t('Appearence'),
      '#group' => 'map_route',
    ];

    // Appearence tab - sub vertical_tabs who's named "appearance_vertical_tabs".
    $form['appearance']['appearance_vertical_tabs'] = [
      '#type' => 'vertical_tabs',
      '#default_tab' => 'edit-dimension-map',
    ];

    // Appearence tab - Dimension Map sub-tab.
    $form['appearance']['dimension_map'] = [
      '#type' => 'details',
      '#title' => $this->t('Map Dimension'),
      '#group' => 'appearance_vertical_tabs',
    ];

    // Appearence tab - Destination sub-tab - Rewrite some fields.
    if (!empty($form['appearance_dimension_map_width_map'])) {
      $form['appearance']['dimension_map']['width_map'] = $form['appearance_dimension_map_width_map'];
      unset($form['appearance_dimension_map_width_map']);
    }
    if (!empty($form['appearance_dimension_map_height_map'])) {
      $form['appearance']['dimension_map']['height_map'] = $form['appearance_dimension_map_height_map'];
      unset($form['appearance_dimension_map_height_map']);
    }

    // Appearence tab - Form Position sub-tab.
    $form['appearance']['form_position'] = [
      '#type' => 'details',
      '#title' => $this->t('Form Settings : Position management'),
      '#group' => 'appearance_vertical_tabs',
    ];

    // Appearence tab - Form Position sub-tab - Rewrite some fields.
    if (!empty($form['appearance_form_position_top_position'])) {
      $form['appearance']['form_position']['top_position'] = $form['appearance_form_position_top_position'];
      unset($form['appearance_form_position_top_position']);
    }
    if (!empty($form['appearance_form_position_bottom_position'])) {
      $form['appearance']['form_position']['bottom_position'] = $form['appearance_form_position_bottom_position'];
      unset($form['appearance_form_position_bottom_position']);
    }
    if (!empty($form['appearance_form_position_left_position'])) {
      $form['appearance']['form_position']['left_position'] = $form['appearance_form_position_left_position'];
      unset($form['appearance_form_position_left_position']);
    }
    if (!empty($form['appearance_form_position_right_position'])) {
      $form['appearance']['form_position']['right_position'] = $form['appearance_form_position_right_position'];
      unset($form['appearance_form_position_right_position']);
    }

    // Appearence tab - Form Color text sub-tab.
    $form['appearance']['form_color_text'] = [
      '#type' => 'details',
      '#title' => $this->t('Form Color Settings : Text'),
      '#group' => 'appearance_vertical_tabs',
    ];

    // Appearence tab - Form Color text sub-tab - Rewrite some fields.
    if (!empty($form['appearance_form_color_text_label_text_color'])) {
      $form['appearance']['form_color_text']['label_text_color'] = $form['appearance_form_color_text_label_text_color'];
      unset($form['appearance_form_color_text_label_text_color']);
    }
    if (!empty($form['appearance_form_color_text_submit_button_text_color'])) {
      $form['appearance']['form_color_text']['submit_button_text_color'] = $form['appearance_form_color_text_submit_button_text_color'];
      unset($form['appearance_form_color_text_submit_button_text_color']);
    }

    // Appearence tab - Form Color Background sub-tab.
    $form['appearance']['form_color_bg'] = [
      '#type' => 'details',
      '#title' => $this->t('Form Color Settings : Background'),
      '#group' => 'appearance_vertical_tabs',
    ];

    // Appearence tab - Form Color Background sub-tab - Rewrite some fields.
    if (!empty($form['appearance_form_color_bg_header_color'])) {
      $form['appearance']['form_color_bg']['header_color'] = $form['appearance_form_color_bg_header_color'];
      unset($form['appearance_form_color_bg_header_color']);
    }
    if (!empty($form['appearance_form_color_bg_form_color'])) {
      $form['appearance']['form_color_bg']['form_color'] = $form['appearance_form_color_bg_form_color'];
      unset($form['appearance_form_color_bg_form_color']);
    }
    if (!empty($form['appearance_form_color_bg_button_color'])) {
      $form['appearance']['form_color_bg']['button_color'] = $form['appearance_form_color_bg_button_color'];
      unset($form['appearance_form_color_bg_button_color']);
    }

    // Appearence tab - Form Color Three Btn sub-tab.
    $form['appearance']['form_color_three_btn'] = [
      '#type' => 'details',
      '#title' => $this->t('Form Color Settings : BTN switch & BTN Minimize Form & BTN Restore Form '),
      '#group' => 'appearance_vertical_tabs',
    ];

    // Appearence tab - Form Color Three Btn sub-tab - Rewrite some fields.
    if (!empty($form['appearance_form_color_three_btn_three_btn_color'])) {
      $form['appearance']['form_color_three_btn']['three_btn_color'] = $form['appearance_form_color_three_btn_three_btn_color'];
      unset($form['appearance_form_color_three_btn_three_btn_color']);
    }
    if (!empty($form['appearance_form_color_three_btn_three_btn_hover_color'])) {
      $form['appearance']['form_color_three_btn']['three_btn_hover_color'] = $form['appearance_form_color_three_btn_three_btn_hover_color'];
      unset($form['appearance_form_color_three_btn_three_btn_hover_color']);
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    $entity = $this->getEntity();
    if (true) {
      $this->mapRouteFactory->updateAppropriateLocationField($entity);
    }
    $result = $entity->save();
    $link = $entity->toLink($this->t('View'))->toRenderable();

    $message_arguments = ['%label' => $this->entity->label()];
    $logger_arguments = $message_arguments + ['link' => render($link)];

    if ($result == SAVED_NEW) {
      $this->messenger()->addStatus($this->t('New map route %label has been created.', $message_arguments));
      $this->logger('map_route_planner')->notice('Created new map route %label', $logger_arguments);
    } else {
      $this->messenger()->addStatus($this->t('The map route %label has been updated.', $message_arguments));
      $this->logger('map_route_planner')->notice('Updated new map route %label.', $logger_arguments);
    }

    $form_state->setRedirect('entity.map_route.canonical', ['map_route' => $entity->id()]);
  }

}
