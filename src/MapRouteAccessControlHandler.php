<?php

namespace Drupal\map_route_planner;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the map route entity type.
 */
class MapRouteAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {

    switch ($operation) {
      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'view map route');

      case 'update':
        return AccessResult::allowedIfHasPermissions($account, ['edit map route', 'administer map route'], 'OR');

      case 'delete':
        return AccessResult::allowedIfHasPermissions($account, ['delete map route', 'administer map route'], 'OR');

      default:
        // No opinion.
        return AccessResult::neutral();
    }

  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermissions($account, ['create map route', 'administer map route'], 'OR');
  }

}
