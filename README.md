# map_route_planner
## V2 of calculate_route !!!
## See step 3 for make a rapid install.

# 0 - Needs the following patch to applicate on drupal/core (for save lat and lng values).
## "https://www.drupal.org/project/drupal/issues/2230909: Simple decimals fail to pass validation": "https://www.drupal.org/files/issues/2022-04-19/2230909-251.patch"

# 1 - Needs the module color_field and field_group and install the previous patch.
## composer require drupal/color_field drupal/field_group; drush en color_field field_group -y

# 2 - Needs the module twig_tweak (JUST FOR DEVELOPMENT).
## composer require drupal/twig_tweak; drush en twig_tweak -y

# 3 - Rapid install.
## 1. Put in the patch of drupal/core => "https://www.drupal.org/project/drupal/issues/2230909: Simple decimals fail to pass validation": "https://www.drupal.org/files/issues/2022-04-19/2230909-251.patch"
## 2. composer require drupal/color_field drupal/field_group drupal/twig_tweak;
## 2b. composer install (optional because the composer require make install too !)
## 3. drush en twig_tweak map_route_planner -y

# 4 - Rapid uninstall the all from the root Project.
## cd app; drush pmu twig_tweak color_field field_group map_route_planner; gch composer.*;composer install; cd ..;
